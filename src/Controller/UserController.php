<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function index(): Response
    {
        return $this->render('user/list.html.twig', [
            'controller_name' => 'ProjectController',
        ]);
    }
    
    /**
     *@Route("/create-user", name="create_user_form", methods={"GET"})
     */
    public function userForm()
    {
       return $this->render('/user/addUser.html.twig');
    }
     /**
     * @Route("/create-user", name="user_add")
     * @param Request $request
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request, SessionInterface $session, EntityManagerInterface $entityManager)
    {
        if ($request->getMethod() == 'GET') {
            return $this->render('user/create-user.html.twig');
        }
        if ($request->getMethod() == 'POST') {
            $user = new User();
            $user->setEmail($request->request->get('email'));
            $user->setLastname($request->request->get('lastname'));
            $user->setFirstname($request->request->get('firstname'));
            $user->setPhone($request->request->get('phone'));
            $user->setSiren($request->request->get('siren'));
            $user->setGarages($request->request->get('garages'));
            $user->setUsername($request->request->get('username'));
            $user->setPassword(password_hash($request->request->get('password'), PASSWORD_BCRYPT));

            $entityManager->persist($user);
            $entityManager->flush();

            // stocker l'user en session pour savoir s'il est connecté ou non
            $session->set('user', $user);
            return new Response('Utilisateur Ajoutée');
        }
    }
 
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="user_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

}
