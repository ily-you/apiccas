<?php

namespace App\Controller;

use App\Entity\Garage;
use App\Repository\UserRepository;
use App\Repository\GarageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GarageController extends AbstractController
{
  /**
     * @Route("/garage", name="garage")
     */
    public function index(): Response
    {
        return $this->render('garages/add.html.twig', [
            'controller_name' => 'ProjectController',
        ]);
    }
    /**
     * @Route("/add_garage/{id}", name="garage_add")
     * @param Request $request
     */
    public function add($id,UserRepository $userRepository, Request $request, EntityManagerInterface $entityManager)
    {
        $user = $userRepository->find($id);
        if ($request->getMethod() === 'POST') {
            $garage = new Garage();
            $garage->setName($request->request->get('name'));
            $garage->setStreet($request->request->get('street'));
            $garage->setStreetComplement($request->request->get('streetcomplement'));
            $garage->setPostalCode($request->request->get('postal'));
            $garage->setSiret($request->request->get('siret'));
            $garage->setCity($request->request->get('city'));
            $garage->setUser($user);

            $entityManager->persist($garage);
            $entityManager->flush();
            return $this->redirectToRoute('garage_list');
            
        }
        return $this->render('garages/add.html.twig', ['user' => $user]);
        
    }
      /**
     *@Route("/", name="garage_list")
     */
    public function liste( GarageRepository $garageRepository): Response
    {
        $garages =  $garageRepository->findAll();
        return $this->render('garages/list.html.twig', ['garages' => $garages]);

    }
  
}